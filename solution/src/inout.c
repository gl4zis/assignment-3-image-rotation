#include "inout.h"
#include "bmp.h"
#include <stdio.h>

read_status get_bmp_image(char* const filename, struct image* img) {
    if (!filename)
        return READ_INVALID_FILENAME;
    FILE* file = fopen(filename, "rb");
    if (!file)
        return READ_INVALID_FILENAME;
    int status = from_bmp(file, img);
    fclose(file);
    return status;
}

write_status write_bmp_image(char* filename, struct image* const img) {
    if (!filename)
        return WRITE_ERROR;
    FILE* file = fopen(filename, "wb");
    if (!file)
        return WRITE_ERROR;
    int status = to_bmp(file, img);
    fclose(file);
    return status;
}
