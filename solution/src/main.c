#include "inout.h"
#include "rotation.h"
#include "validation.h"
#include <stdio.h>
#include <stdlib.h>

int main( int argc, char** argv ) {
    char* src_img;
    char* trg_img;
    rotate_angle angle;
    int status = validate_cmd_args(argc, argv, &src_img, &trg_img, &angle);
    if (status != VALID) {
        fprintf(stderr, "Validation Failed with code: %d\n", status);
        return status;
    }

    struct image img = {0};
    status = get_bmp_image(src_img, &img);
    if (status != READ_OK) {
        free(img.data);
        fprintf(stderr, "Reading Failed with code: %d\n", status);
        return status;
    }

    img = rotate_on_angle(img, angle);
    status = write_bmp_image(trg_img, &img);
    if (status != WRITE_OK) {
        free(img.data);
        fprintf(stderr, "Writing Failed with code: %d\n", status);
        return status;
    }
    
    free(img.data);
    printf("OK\n");
    return status;
}
