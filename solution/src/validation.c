#include "validation.h"
#include "util.h"
#include <stdlib.h>

valid_status validate_cmd_args(int const count, char** const args,
    char** const src_img, char** const trg_img, rotate_angle* const angle)
{
    if (count != 4)
        return INVALID_NUMBER_OF_OPERANDS;
    *src_img = args[1];
    *trg_img = args[2];
    if (!is_num(args[3]))
        return INVALID_ANGLE;
    char* p = args[3];
    while (*p) {
        p++;
    }
    if (p - args[3] > 5)
        return INVALID_ANGLE;
    int16_t raw_angle = (int16_t) strtol(args[3], &p, 10);
    if (raw_angle < 0)
        raw_angle += 360;
    if (raw_angle >= 360)
        raw_angle %= 360;
    if (raw_angle % 90 == 0) {
        *angle = raw_angle / 90;
        return VALID;
    } else
        return INVALID_ANGLE;
}
