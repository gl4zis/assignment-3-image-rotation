#include "image.h"
#include "util.h"
#include <stdlib.h>

uint64_t get_data_index(uint32_t width, uint32_t row, uint32_t col) {
    return row*width + col;
}

uint32_t get_image_size(struct image img) {
    return round_mod_4(img.width*sizeof(struct pixel)) * img.height;
}

struct pixel pixel_create(char* const table) {
    return (struct pixel) {table[0], table[1], table[2]};
}

static uint32_t extra_pxls_in_row(uint32_t width) {
    return round_mod_4(width*sizeof(struct pixel)) - width*sizeof(struct pixel);
}

struct image image_create(uint32_t width, uint32_t height, char* table) {
    uint32_t extra_pxls = extra_pxls_in_row(width);
    struct pixel* pixels = malloc(width*height*sizeof(struct pixel)); 
    struct image img = (struct image) {width, height, pixels};
    for (uint32_t i = 0; i < height; i++) {
        for (uint32_t j = 0; j < width; j++) {
            pixels[get_data_index(width, i, j)] = pixel_create(table);
            table += sizeof(struct pixel);
        }
        table += extra_pxls;
    }
    return img;
}

char* pixels_to_chars(struct image img) {
    char* const chars = malloc(get_image_size(img));
    const uint64_t row_len = round_mod_4(img.width*sizeof(struct pixel));
    uint32_t extra_pxls = extra_pxls_in_row(img.width);
    const struct pixel* data = img.data;
    const uint32_t width = img.width;
    for (uint32_t i = 0; i < img.height; i++) {
        for (uint32_t j = 0; j < width; j++) {
            chars[i*row_len + j*sizeof(struct pixel)] = (char) data[i*width + j].r;
            chars[i*row_len + j*sizeof(struct pixel) + 1] = (char) data[i*width + j].g;
            chars[i*row_len + j*sizeof(struct pixel) + 2] = (char) data[i*width + j].b;
        }
        for (uint32_t j = 0; j < extra_pxls; j++)
            chars[i*row_len + width*sizeof(struct pixel) + j] = 0;
    }
    return chars;
}
