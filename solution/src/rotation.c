#include "rotation.h"
#include "error_statuses.h"
#include <stdlib.h>

static struct image rotate(struct image const source) {
    const uint32_t width = source.width;
    const uint32_t height = source.height;
    struct image new_image = {0};
    new_image.width = height;
    new_image.height = width;
    new_image.data = malloc(width * height * sizeof(struct pixel));
    for (int32_t row = 0; row < height; row++) {
        for (int32_t col = 0; col < width; col++)
            new_image.data[get_data_index(height, width-col-1, row)] = source.data[get_data_index(width, row, col)];
    }
    free(source.data);
    return new_image;
}

struct image rotate_on_angle(struct image img, rotate_angle angle) {
    for (rotate_angle i = 0; i < angle; i++)
        img = rotate(img);
    return img;
}
