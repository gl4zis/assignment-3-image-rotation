#include "util.h"
#include <string.h>

uint64_t round_mod_4(uint64_t num) {
    if (num % 4 == 0)
        return num;
    else
        return num + 4 - num%4;
}

bool is_num(char* const str) {
    if (!str)
        return false;
    if (str[0] != '-' && (str[0] > '9' || str[0] < '0'))
        return false;
    for (size_t i = 1; i < strlen(str); i++) {
        if (str[i] > '9' || str[i] < '0')
            return false;
    }
    return true;
}
