#include "bmp.h"
#include "util.h"
#include <stdlib.h>

#define BF_TYPE 19778
#define B_OFF_BITS 54
#define BI_SIZE 40
#define BI_PLANES 1
#define BI_BIT_COUNT 24
#define PELS_PER_METER 0

struct __attribute__((packed)) bmp_header
{
    uint16_t bfType;
    uint32_t  bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t  biHeight;
    uint16_t  biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t  biClrImportant;
};

read_status from_bmp( FILE* in, struct image* img ) {
    struct bmp_header header = {0};
    if (fread(&header, sizeof(struct bmp_header), 1, in) != 1)
        return READ_INVALID_HEADER;

    char* table = malloc(header.biSizeImage);
    if (fread(table, header.biSizeImage, 1, in) != 1) {
        free(table);
        return READ_INVALID_DATA;
    }
    *img = image_create(header.biWidth, header.biHeight, table);
    free(table);
    return READ_OK;
}

static void standard_header_init(struct bmp_header* const header, struct image const* img)
{
    uint32_t img_size = get_image_size(*img);
    header->bfType = BF_TYPE;
    header->bfileSize = img_size + B_OFF_BITS;
    header->bfReserved = 0;
    header->bOffBits = B_OFF_BITS;
    header->biSize = BI_SIZE;
    header->biWidth = img->width;
    header->biHeight = img->height;
    header->biPlanes = BI_PLANES;
    header->biBitCount = BI_BIT_COUNT;
    header->biCompression = 0;
    header->biSizeImage = img_size;
    header->biXPelsPerMeter = PELS_PER_METER;
    header->biYPelsPerMeter = PELS_PER_METER;
    header->biClrUsed = 0;
    header->biClrImportant = 0;
}

write_status to_bmp( FILE* out, struct image const* img ) {
    struct bmp_header header = {0};
    standard_header_init(&header, img);

    if (fwrite(&header, sizeof(struct bmp_header), 1, out) != 1)
        return WRITE_ERROR;

    const uint32_t img_size = get_image_size(*img);
    char* table = pixels_to_chars(*img);
    if (fwrite(table, 1, img_size, out) != img_size) {
        free(table);
        return WRITE_ERROR;
    }

    free(table);
    return WRITE_OK;
}
