#include "error_statuses.h"
#include "image.h"
#include <stdint.h>
#include <stdio.h>

#ifndef BMP_H
#define BMP_H

read_status from_bmp( FILE* in, struct image* img );

write_status to_bmp( FILE* out, struct image const* img );

#endif
