#include "error_statuses.h"
#include "image.h"

#ifndef INOUT_H
#define INOUT_H

read_status get_bmp_image(char* const filename, struct image* img);
write_status write_bmp_image(char* const filename, struct image* const img);

#endif
