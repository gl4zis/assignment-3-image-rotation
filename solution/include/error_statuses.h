#ifndef ERRORS_H
#define ERRORS_H

typedef enum {
  READ_OK = 0,
  READ_INVALID_FILENAME,
  READ_INVALID_HEADER,
  READ_INVALID_DATA
} read_status;

typedef enum {
  WRITE_OK = 0,
  WRITE_ERROR
} write_status;

typedef enum {
  VALID = 0,
  INVALID_NUMBER_OF_OPERANDS,
  INVALID_ANGLE
} valid_status;

#endif
