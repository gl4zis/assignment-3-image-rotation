#include "image.h"
#include <stdbool.h>

#ifndef ROTATION_H
#define ROTATION_H

typedef enum {
    A_0 = 0,
    A_90,
    A_180,
    A_270
} rotate_angle;

struct image rotate_on_angle(struct image const img, rotate_angle angle);

#endif
