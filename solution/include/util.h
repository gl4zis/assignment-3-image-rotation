#include <stdbool.h>
#include <stdint.h>

#ifndef UTIL_H
#define UTIL_H

uint64_t round_mod_4(uint64_t num);

bool is_num(char* const str);

bool parse_number(char* const str, int16_t* num);

#endif
