#include <stdint.h>

#ifndef IMAGE_H
#define IMAGE_H

struct pixel {
    uint8_t r, g, b;
};

struct image {
    uint32_t width, height;
    struct pixel* data;
};

struct pixel pixel_create(char* const table);

struct image image_create(uint32_t width, uint32_t height, char* rastr_table);

char* pixels_to_chars(struct image img);

uint64_t get_data_index(uint32_t width, uint32_t row, uint32_t col);

uint32_t get_image_size(struct image img);

#endif
