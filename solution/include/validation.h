#include "error_statuses.h"
#include "rotation.h"
#include <stdint.h>

#ifndef VALIDATION_H
#define VALIDATION_H

valid_status validate_cmd_args(int const count, char** const args,
    char** const src_img, char** const trg_img, rotate_angle* const angle);

#endif
